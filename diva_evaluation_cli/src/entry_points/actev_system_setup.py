"""Entry point module: system-setup

Implements the entry-point by using Python or any other languages.

"""
import os

def entry_point():
    """Method to complete: you have to raise an exception if an error occured in the program.

    Run any compilation/preparation steps for the system.

    """
    raise NotImplementedError("You should implement the entry_point method.")
